// 合并多次setData, 延迟策略: [setData大于待于10次] 或者 [一秒之内setData多次] 合并为一次
// 使用方式: var setMyData = $mySetData(this);
// setMyData({key: xxx, value: yyyy}); setMyData({key: 222, value: 3333});
export const $mySetData = function (page) {
    let setData_set = {};
    let setData_time = null;
    let setData_setTimeOut = null;

    return function (obj) {
        setData_set = Object.assign(setData_set, obj);
        if (setData_time == null) {
            setData_time = new Date().getTime();
        }
        let count = 0;
        for (let k in setData_set) {
            count += 1;
        }
        if (count >= 10) {
            if (JSON.stringify(setData_set) == '{}') return;

            //想抽出来的代码, 但因为闭包特性, 抽成方法后变量会复制一份,所以作罢
            //console.log("setData", setData_set);
            page.setData(setData_set);
            setData_time = null;
            setData_set = {};
            setData_setTimeOut = null;

            return;
        }
        if (setData_setTimeOut) return;
        setData_setTimeOut = setTimeout(function () {
            if (JSON.stringify(setData_set) == '{}') return;

            //想抽出来的代码, 但因为闭包特性, 抽成方法后变量会复制一份,所以作罢
            //console.log("setTimeout", setData_set);
            page.setData(setData_set);
            setData_time = null;
            setData_set = {};
            setData_setTimeOut = null;

        }, 500)
    }
}
