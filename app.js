App({
  loadFont: function loadFont() {
    wx.loadFontFace({
      family: '思源黑体',
      source: 'url("https://guoyu.link/static/font/webfont_siyuan.ttf")',
      success: function (res) {
        //console.log("字体加载成功(思源黑体)", res);
      }
    });
  },
  onLaunch: function () {
    //this.loadFont();
    // develop, trial, release
    // console.log("envVersion", __wxConfig.envVersion)
  }
})
