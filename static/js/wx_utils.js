/**
 * 获得顶部导航高度
 * @returns {*}
 */
export const $getTopNavHeight = function(){
    const config = wx.getSystemInfoSync();
    let navHeight = config.statusBarHeight + 46;
    return navHeight;
}

/**
 * 返回上级
 * @param backUrl
 */
export const $navigateBack = function(backUrl){
    let pages = getCurrentPages();
    if (pages.length >= 2) {
        let p = pages[pages.length - 2];
        let route = p.route;
        wx.navigateBack({
            delta: 1
        })
        return;
    }
    wx.navigateTo({
        url: (backUrl || "/index/index"),
    })
}


export const $getOption = function(key) {
    var pages = getCurrentPages();
    var currentPage = pages[pages.length - 1];
    var options = currentPage.options;
    if (key) {
        var v = options[key];
        return decodeURIComponent(v);
    }
    return options;
}
