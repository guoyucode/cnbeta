import {$page_htmlToJson} from "../static/js/cnbeta_htmlToJson"
import {$getOption} from "../static/js/wx_utils"

Page({
    data: {
        imgList: [],
        loadingImg: "/static/img/loadingImg.gif",
        errImg: "/static/img/errimg2.jpg",
        page: {},
        userInfo: {},
        nav: null,
    },

    //图片加载成功
    loadSuccess(e) {
        let i = e.currentTarget.dataset.i;
        this.setData({["page.contents[" + i + "].showImg"]: true});
    },

    //图片加载失败
    loadError(e) {
        let self = this;
        let i = e.currentTarget.dataset.i;
        let v = e.currentTarget.dataset.v;
        let retry = v.retry || 0;

        if (v.x == '' && v.orgImg != '') {
            return;
        }

        if (retry < 3) {
            setTimeout(function () {
                v.showImg = false;
                v.retry = retry = retry + 1;
                v.x = v.orgImg + "?t=" + new Date().getTime();
                self.setData({["page.contents[" + i + "]"]: v});
            }, 1000 * retry)
            return;
        }
        ;
        console.log("图片加载失败,已经重试3次", v);
        v.showImg = true;
        v.x = this.data.errImg;
        this.setData({["page.contents[" + i + "]"]: v});
    },

    backCallback() {
        this.imgList = [];
        this.page = {};
    },

    //刷新方法, 点击标题时可调用
    onRefresh() {
        this.requsetNews();
    },

    /**
     * 预览图片
     */
    previewImage(e) {
        let self = this;
        let i = e.currentTarget.dataset.i;
        let v = e.currentTarget.dataset.v;
        wx.previewImage({
            current: v.x, // 当前显示图片的http链接
            urls: this.data.page.imgList // 需要预览的图片http链接列表
        });
    },

    requsetNews() {
        wx.showLoading({title: '加载中...'});
        const self = this;
        var href = $getOption("affection");
        if (!href || href == "undefined") {
            wx.hideLoading();
            wx.showToast({
                title: "加载数据失败! 链接错误,请重新进入.",
                duration: 5000,
                mask: true,
                icon: "none"
            });
            return;
        }
        ;
        wx.request({
            url: href,
            success: function success(res) {
                const p = $page_htmlToJson(res.data);
                self.setData({page: p}, function () {
                    wx.hideLoading();
                    self.wacthImgView(p.contents);
                    wx.showShareMenu();
                });

            },
            fail: function fail(res) {

                setTimeout(function () {
                    wx.hideLoading();
                    wx.showModal({
                        title: '加载数据失败!',
                        content: '您的网络不太好, 请选择是否重试 ?',
                        showCancel: true,//是否显示取消按钮
                        cancelText: "否",//默认是“取消”
                        confirmText: "是",//默认是“确定”
                        success: function (res) {
                            if (res.cancel) return;//点击取消,默认隐藏弹框
                            self.requsetNews();
                        }
                    });
                }, 1500)
            }
        });
    },

    //监听图片显示
    wacthImgView(list) {
        const self = this;
        for (let k in list) {
            let v = list[k];
            if (!v.myClass || v.isWatch || v.t != 'img') continue;
            let wxInObj = wx.createIntersectionObserver();
            v.isWatch = true;
            wxInObj.relativeToViewport().observe("#" + v.myClass, function (res) {
                if (res.intersectionRatio > 0) {
                    v.showImg = false;
                    v.x = v.orgImg;
                    self.setData({["page.contents[" + k + "]"]: v});
                    wxInObj.disconnect();
                }
            });
        }
    },

    clickHandle(msg, ev) {
        //console.log('clickHandle:', msg, ev)
    },

    onShareAppMessage(res) {
        var pages = getCurrentPages();
        var currentPage = pages[pages.length - 1];
        var url = currentPage.route;
        url += "&affection=" + $getOption("affection");
        console.log("url", url)
        return {
            path: "/index/index?to=/" + url,
            title: this.data.page.title,
        };
    },

    onLoad() {
        wx.showLoading({title: '加载中...'});
    },

    //生命周期 - 页面渲染完成
    onReady(){
        //this.requsetNews();
    },

    onShow(){
        //如果有数据就不用渲染了, 网络不好没有加载到页面的情况
        if(JSON.stringify(this.data.page) != "{}") return;
        this.requsetNews();
    },

    //页面卸载: 页面返回时
    onUnload() {
        this.imgList = [];
        this.page = {};
    }

});
